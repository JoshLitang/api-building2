﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Might : Buff
{
    Stats stats;
    public float amount;
    
    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        stats = target.GetComponent<Stats>();
        Effect();
    }

    // Update is called once per frame
    protected override void Update()
    {

    }

    protected override void Effect()
    {
        stats.power += amount;
    }
}
