﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Poison : Buff
{
    Player player;
    public float amount;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        player = target.GetComponent<Player>();
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }

    protected override void Effect()
    {
        player.currentHp -= (player.maxHp * amount);
    }
}
