﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vigor : Buff
{
    Stats stats;
    public float amount;
    float originalVit;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        stats = target.GetComponent<Stats>();
        originalVit = stats.vitality;
        Effect();
    }

    // Update is called once per frame
    protected override void Update()
    {
        duration -= Time.deltaTime;
        if (duration <= 0) Remove();
    }

    protected override void Effect()
    {
        stats.vitality += (stats.vitality * amount);
    }

    public override void Remove()
    {
        stats.vitality = originalVit;
        base.Remove();
    }
}
