﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slot : MonoBehaviour
{
    //number of current stacks
    public int Stacks;
    //The inventory item
    public Item Stored;

    public void AddItem(Item obj)
    {
        Stored = obj;
        AddStack();
    }

    public void AddStack()
    {
        Stacks++;
        Stacks = Mathf.Clamp(Stacks, 1, Stored.maxStacks);
    }
}
