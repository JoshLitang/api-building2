﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemTypes
{
    stackingUsable,
    stackingNonUsable,
    nonStackingUsable,
    nonStackingNonUsable
}

public class Item : MonoBehaviour
{
    [Header("Item Information")]
    public string itemID;
    public string itemName;
    public ItemTypes types;
    public Sprite itemIcon;

    [Header("Item Status")]
    public int maxStacks;
    public int stacks;

    protected virtual void Start()
    {

    }

    protected virtual void Update()
    {

    }

    public virtual void UseItem()
    {

    }

    public virtual void Remove()
    {
        Destroy(this);
    }
}
