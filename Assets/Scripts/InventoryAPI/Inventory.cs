﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Inventory : MonoBehaviour
{
    public Item InvItem;
    public int SlotLimit;
    public ItemTypes Types;
    public List<Slot> Stack;

    private void Start()
    {
        
    }

    // Adds Slots to inventory list
    public void AddToSlot(Slot obj) {

        //initialization
        if (Stack == null) {
            Slot Stack = new Slot();
        }

        switch (obj.Stored.types) {
            case ItemTypes.nonStackingNonUsable:
                NonUsableStack(obj);
                break;
            case ItemTypes.stackingNonUsable:
                
                break;
            case ItemTypes.stackingUsable:
                
                break;
            case ItemTypes.nonStackingUsable:
                
                break;
        }
    }

    //Removes a slot at an index
    public void RemoveSlot(int index) {
        
        Stack.RemoveAt(index);
    }

    //checks if same item exists and then returns the item's index
    public int IsInSlot(Slot space) {
        foreach (Slot s in Stack)
        {
            if (s.Stored.itemID == InvItem.itemID)
            {
                if (Stack.Count() >= SlotLimit)
                {

                }
            }
        }
        return 1;
    }

    //Checks if the Slot's stacks are full
    public void UsableStack(Slot space) {
        //checking for a duplicate
        Slot original = Stack.Where(s => s.Stored.itemID == space.Stored.itemID).SingleOrDefault();
        bool capped = Stack.Count() >= SlotLimit ? true : false;
        if (original)
        {
            space.Stacks = 1 + original.Stacks;
            Stack.Remove(original);
            Stack.Add(space);
        }
        
        if (!capped) {
            if (Stack.Count() >= SlotLimit)
            {
                space.AddStack();
                Stack.Add(space);
            }
        }
    }

    //Item Stacks but cannot be used
    public void NonUsableStack(Slot space)
    {
        if (Stack.Count() < SlotLimit)
        {
            space.AddStack();
            Stack.Add(space);
        }
    }
}
