﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuffSlot : MonoBehaviour
{
    public Text buffText;
    public Text buffDuration;
    public Text buffStack;
    public Sprite buffSprite;

    string buffName;
    float duration;
    int stack;
    Sprite sprite;

    void Start()
    {
        buffSprite = sprite;
        buffText.text = buffName;
    }


    void Update()
    {
        buffDuration.text = duration.ToString();
        buffStack.text = stack.ToString();
    }
}
