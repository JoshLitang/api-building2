﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUIManager : MonoBehaviour
{
    public GameObject player;

    public Text health;
    public Text pow;
    public Text prec;
    public Text tough;
    public Text vit;

    void Update()
    {
        health.text = "Current Health: " + player.GetComponent<Player>().currentHp.ToString();
        pow.text = "Power: " + player.GetComponent<Player>().stats.power.ToString();
        prec.text = "Precision: " + player.GetComponent<Player>().stats.precision.ToString();
        tough.text = "Toughness: " + player.GetComponent<Player>().stats.toughness.ToString();
        vit.text = "Vitality: " + player.GetComponent<Player>().stats.vitality.ToString();
    }
}
