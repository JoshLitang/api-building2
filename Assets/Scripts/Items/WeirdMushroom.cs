﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeirdMushroom : Item
{
    public GameObject buffObject;
    public float amount = 0.05f;
    GameObject player;
    Stats Stats;

    protected override void Start()
    {
        player = FindObjectOfType<Player>().gameObject;
        Stats = player.GetComponent<Stats>();
    }

    protected override void Update()
    {
        base.Update();
    }

    public override void UseItem()
    {
        int roll = Random.Range(1, 100);

        if (roll <= 20)
        {
            player.GetComponent<Player>().currentHp += (player.GetComponent<Player>().maxHp * amount);
        }
        else
        {
            Instantiate(buffObject, transform.position, Quaternion.identity);
        }
    }
}
