﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarriorsEmblem : Item
{
    public GameObject buffObject;

    protected override void Start()
    {
        UseItem();
    }

    protected override void Update()
    {

    }

    public override void UseItem()
    {
        Instantiate(buffObject, transform.position, Quaternion.identity);
    }

}
