﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RejuvenatingPotion : Item
{
    public GameObject buffObject;

    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        base.Update();
    }

    public override void UseItem()
    {
        Instantiate(buffObject, transform.position, Quaternion.identity);
    }
}
