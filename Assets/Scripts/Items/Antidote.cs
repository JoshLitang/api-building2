﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Antidote : Item
{
    public BuffReciever actor;
    public GameObject buffObject;

    protected override void Start()
    {
        actor = FindObjectOfType<BuffReciever>();
    }

    public override void UseItem()
    {
        actor.RemoveBuff(buffObject.GetComponent<Buff>().buffID);
    }
}
