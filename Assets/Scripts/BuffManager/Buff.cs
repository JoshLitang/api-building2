﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BuffType
{
    Duration,
    Intensity,
    Override,
    None
}

public class Buff : MonoBehaviour
{
    [Header("Buff Information")]
    public string buffID;
    public string buffName;
    public BuffType type;
    public Sprite buffIcon;

    [Header("Buff Status")]
    public int maxStacks;
    public int stacks = 0;
    public float duration;
    public float maxTick = 1;
    float currentTick;

    public BuffReciever target;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        currentTick = maxTick;

        // Find Player object with BuffReciever then register and apply the buff. Set the buff object as a child of the Player object
        target = FindObjectOfType<Player>().GetComponent<BuffReciever>();
        target.ApplyBuff(this);
        transform.SetParent(target.gameObject.transform);
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        duration -= Time.deltaTime;
        currentTick -= Time.deltaTime;
        if (duration <= 0)
        {
            Remove();
        }

        if (currentTick <= 0)
        {
            currentTick = maxTick;
            Effect();
        }
    }

    protected virtual void Effect()
    {

    }

    public virtual void Remove()
    {
        Destroy(this);
    }
}
