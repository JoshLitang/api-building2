﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Events;
using System;

public class BuffReciever : MonoBehaviour
{
    public List<Buff> buffs = new List<Buff>();

    public void ApplyBuff(Buff buff)
    {
        switch (buff.type)
        {
            case BuffType.Duration:
                AddDuration(buff);
                break;
            case BuffType.Intensity:
                AddStacks(buff);
                break;
            case BuffType.Override:
                OverrideBuff(buff);
                break;
            case BuffType.None:
                ApplyBuff(buff);
                break;
        }
    }

    // Adds the new buff to the list
    void RegisterBuff(Buff buff)
    {
        buffs.Add(buff);
    }

    // Checks first if the buff is already in the list and will replace the existing buff
    public void OverrideBuff(Buff buff)
    {
        Buff original = buffs.Where(s => s.buffID == buff.buffID).SingleOrDefault();
        if (original)
        {
            buffs.Remove(original);
            ApplyBuff(buff);
        }
        else
            ApplyBuff(buff);
    }

    // Checks first if the buff is already in the list and will add the existing duration to the new buff instance
    public void AddDuration(Buff buff)
    {
        Buff original = buffs.Where(s => s.buffID == buff.buffID).SingleOrDefault();
        if (original)
        {
            buff.duration += original.duration;
            buffs.Remove(original);
        }

        ApplyBuff(buff);
    }

    // Checks first if the buff is already in the list and is stackable
    public void AddStacks(Buff buff)
    {
        // Apply buff effect if there's still room for new stacks
        int newStacks = GetStacks(buff.buffID) + buff.stacks;
        if (newStacks <= buff.maxStacks)
        {
            if (newStacks <= buff.maxStacks) RegisterBuff(buff);
            return;
        }

        // Get all exsting buffs and sort them by duration in a list
        List<Buff> existingBuffs = buffs.Where(s => s.buffID == buff.buffID).OrderBy(s => s.duration).ToList();
        foreach (Buff item in existingBuffs)
        {
            // Get the number of stacks needed to be removed and remove buff if its more than the max stack amount
            int stacksToRemove = newStacks - item.maxStacks;
            if (stacksToRemove >= item.maxStacks) item.Remove();
            else
            {
                newStacks -= stacksToRemove;
                item.stacks -= stacksToRemove;
            }

            // Apply new buff after making space for new stacks
            if (newStacks <= buff.maxStacks)
            {
                ApplyBuff(buff);
                return;
            }
        }

        // Get the excess amount of stacks and make room for them to override the existing buffs
        int excess = newStacks - buff.maxStacks;
        if (buff.stacks > excess)
        {
            buff.stacks -= excess;
            ApplyBuff(buff);
            return;
        }

        // Remove buff it wasn't used
        buff.Remove();

    }

    // Removes a specific buff and all instances of it 
    public void RemoveBuff(string id)
    {
        IEnumerable<Buff> buffToRemove = GetBuff(id);

        foreach (Buff buff in buffToRemove)
        {
            buff.Remove();
        }
    }

    // Clears all the buffs in the list
    public void RemoveAllBuffs()
    {
        buffs.Clear();
    }

    // Gets all the buff with the same ID and saves it in a list
    public IEnumerable<Buff> GetBuff(string buffID)
    {
        return buffs.Where(s => s.buffID == buffID).ToList();
    }

    // Gets all the buff with the same ID and gets the total amount
    public int GetStacks(string buffID)
    {
        return buffs.Where(s => s.buffID == buffID).Sum(s => s.stacks);
    }

}
