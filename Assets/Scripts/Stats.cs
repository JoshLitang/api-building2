﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour
{
    public float power;
    public float precision;
    public float toughness;
    public float vitality;
}