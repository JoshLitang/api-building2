﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class BuffUIManager : MonoBehaviour
{
    BuffReciever actor;

    public List<Buff> buffElements = new List<Buff>();

    // Start is called before the first frame update
    void Start()
    {
        actor = FindObjectOfType<BuffReciever>();
        buffElements = actor.buffs;

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    void Refresh()
    {

    }
}
