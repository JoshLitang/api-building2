﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public Stats stats;

    public float maxHp { get { return stats.vitality * 10.0f; } }
    public float currentHp;

    public bool isAlive;

    void Start()
    {
        stats = GetComponent<Stats>();
        currentHp = maxHp;
    }

    void Update()
    {
        if (!isAlive) return;

        if (currentHp <= 0) isAlive = false;
    }
}